//
// Created by florianfrank on 02.07.21.
//

#ifndef MEMORY_TESTING_FW_TEST_CASES_H
#define MEMORY_TESTING_FW_TEST_CASES_H

#endif //MEMORY_TESTING_FW_TEST_CASES_H

#if MEM_ACCESS_IF == SPI
int executeWIPPollingTestAdestoReRam();
#endif // MEM_ACCESS_IF == SPI
int executeReadLatencyTestFRAMRohm();

int executeMemoryTest();
